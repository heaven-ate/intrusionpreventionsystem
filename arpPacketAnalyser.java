
package jpcaptsting;

import java.io.IOException;

import javax.swing.JOptionPane;
import jpcap.*;
public class arpPacketAnalyser extends javax.swing.JFrame {

    
    public arpPacketAnalyser() {
        initComponents();
    }
  Packet_Thread CATN;
    NetworkInterface[]interfaces;
     JpcapCaptor captor;
int index=0,counter=0;
boolean captureState=false;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        Interface = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        select = new javax.swing.JButton();
        capture = new javax.swing.JButton();
        interfaceno = new javax.swing.JTextField();
        stop = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Interface.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Interface.setText("Interface");
        Interface.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InterfaceActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        select.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        select.setText("select");
        select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectActionPerformed(evt);
            }
        });

        capture.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        capture.setText("capture");
        capture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                captureActionPerformed(evt);
            }
        });

        stop.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        stop.setText("stop");
        stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(interfaceno, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Interface)
                                .addGap(36, 36, 36)
                                .addComponent(select)
                                .addGap(29, 29, 29)))
                        .addComponent(capture)
                        .addGap(18, 18, 18)
                        .addComponent(stop))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 962, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(interfaceno, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Interface)
                    .addComponent(select)
                    .addComponent(capture)
                    .addComponent(stop))
                .addGap(192, 192, 192))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    public void capture(){
     
     CATN=new Packet_Thread(){
         
         public Object construct(){
             jTextArea1.setText("new capture at interface"+"  "+index+
                     "\n\n------------------------------------------------------------------------"
                     +"----------------------------------------------------------------\n\n");
             try{
                 captor=JpcapCaptor.openDevice(interfaces[index],65535,false,20);
                 while(captureState){
                     captor.processPacket(1, new  Printer());
                     captor.setFilter("arp ", true);
                  
                   
                 }
               captor.close();
         }
             catch(Exception e){
               JOptionPane.showMessageDialog(null, e);
             }return 0;
}
         public void fnished(){
             this.interrupt();
         }
     };
             
     
    CATN.start();
 }
    
    
    private void InterfaceActionPerformed(java.awt.event.ActionEvent evt) {                                          
        //List network interfaces
        interfaces=JpcapCaptor.getDeviceList();
        //for each network interface
for (int i = 0; i <interfaces.length; i++) {
    counter++;
    jTextArea1.append(i+": "+interfaces[i].name + "(" +interfaces[i].description+")");
  //print out its name and description
  System.out.println(i+": "+interfaces[i].name + "(" +interfaces[i].description+")");

  //print out its datalink name and description
  System.out.println(" datalink: "+interfaces[i].datalink_name + "(" + interfaces[i].datalink_description+")");

  //print out its MAC address
  System.out.print(" MAC address:");
  for (byte b :interfaces[i].mac_address)
    //System.out.print(Integer.toHexString(b&0xff) + ":");
  jTextArea1.append(Integer.toHexString(b&0xff) + ":");
  System.out.println();

  //print out its IP address, subnet mask and broadcast address
  for (NetworkInterfaceAddress a : interfaces[i].addresses)
   // System.out.println(" address:"+a.address + " " + a.subnet + " "+ a.broadcast);
  jTextArea1.append("\n Ip address:"+a.address + "\n subnet mask " + a.subnet + "\n Broadcast address "+ a.broadcast);
}
        
    }                                         

    private void selectActionPerformed(java.awt.event.ActionEvent evt) {                                       
        //select interface 
        int tempo=Integer.parseInt(interfaceno.getText());
        if(tempo>-1&&tempo<counter){
            index=tempo;
            
        }
        else{
            JOptionPane.showMessageDialog(null,"index out of range index#0 to  "+(counter-1)+"is allowed");
        }
        interfaceno.setText("");
    }                                      

    private void captureActionPerformed(java.awt.event.ActionEvent evt) {                                        
      captureState=true;
        capture();
    }                                       

    private void stopActionPerformed(java.awt.event.ActionEvent evt) {                                     
        // TODO add your handling code here:
    }                                    

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(arpPacketAnalyser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(arpPacketAnalyser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(arpPacketAnalyser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(arpPacketAnalyser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new arpPacketAnalyser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton Interface;
    private javax.swing.JButton capture;
    private javax.swing.JTextField interfaceno;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton select;
    private javax.swing.JButton stop;
    // End of variables declaration                   
}
