
package tcp;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import jpcap.*;

public class TCP {
    private  int index;
   private NetworkInterface[]interfaces;
  private  JpcapCaptor captor=null;
 public int getIndex(){
     return index;
 }
 public void setIndex(int newIndex){
 this.index=newIndex;
 }
 
 
    public void openInterface(){
        
          interfaces =JpcapCaptor.getDeviceList();
            //for each network interface
for (int i = 0; i < interfaces.length; i++) {
  //print out its name and description
  System.out.println(i+": "+interfaces[i].name + "(" + interfaces[i].description+")");

  //print out its datalink name and description
  System.out.println(" datalink: "+interfaces[i].datalink_name + "(" + interfaces[i].datalink_description+")");

  //print out its MAC address
  System.out.print(" MAC address:");
  for (byte b : interfaces[i].mac_address)
    System.out.print(Integer.toHexString(b&0xff) + ":");
  System.out.println();

  //print out its IP address, subnet mask and broadcast address
  for (NetworkInterfaceAddress a : interfaces[i].addresses)
    System.out.println(" address:"+a.address + " " + a.subnet + " "+ a.broadcast);
                
            
            
                    } 

    }
    public void selectInterface(){
        try {while(true){
            captor=JpcapCaptor.openDevice(interfaces[index], 65535, false, 20);
           
           
            captor.processPacket(10, new synFloodattackAnalyser());
            captor.close();}
        } catch (IOException ex) {
            Logger.getLogger(TCP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        TCP tcp=new TCP ();
        Scanner s=new Scanner(System.in);
        tcp. openInterface();
        System.out.println("select index");
       int i=s.nextInt();
      tcp. setIndex(i);
      tcp. getIndex();
      tcp.selectInterface();
    }
    
}
